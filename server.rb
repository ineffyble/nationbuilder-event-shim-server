require 'webrick'
require 'webrick/https'
require 'nationbuilder'

NATION = ENV["NATION"]
API_KEY = ENV["API_KEY"]
SITE_SLUG = ENV["SITE_SLUG"]
$cache = {
  "events": {}
}

class NationBuilderInterface

  def self.cache_is_recent(max_mins, timestamp)
    max_seconds = max_mins * 60
    return (Time.now.to_f - timestamp.to_f).to_i
  end

  def self.get_calendars
    if $cache[:calendars] && NationBuilderInterface::cache_is_recent(30, $cache[:calendars]['timestamp'])
      return $cache[:calendars]['items']
    else 
      calendars = {}
      client = NationBuilder::Client.new(NATION, API_KEY)
      results = client.call(:calendars, :index, site_slug: SITE_SLUG, limit: 500)
      results["results"].each do |result, index|
        calendars[result["slug"]] = result["id"]
      end
      $cache[:calendars] = {'items' => calendars, 'timestamp' => Time.now}
      return calendars
    end
  end

  def self.get_all_events
    if $cache[:all_events] && NationBuilderInterface::cache_is_recent(30, $cache[:all_events]['timestamp'])
      return $cache[:all_events]['items']
    else 
      events = {}
      client = NationBuilder::Client.new(NATION, API_KEY)
      now = DateTime.now
      results = client.call(:events, :index, site_slug: SITE_SLUG, starting: now, limit: 500)
      results["results"].each do |result, index|
        nice_date = DateTime.parse(result["start_time"]).strftime("%b %d")
        events[result["name"] + " - " + nice_date] = result["id"]
      end
      $cache[:all_events] = {'items' => events, 'timestamp' => Time.now}
      return events
    end 
  end

  def self.get_events_by_calendar(id)
    if $cache[:events][id] && NationBuilderInterface::cache_is_recent(30, $cache[:events][id]['timestamp'])
      return $cache[:events][id]['items']
    else 
      events = {}
      client = NationBuilder::Client.new(NATION, API_KEY)
      now = DateTime.now
      results = client.call(:events, :index, site_slug: SITE_SLUG, starting: now, calendar_id: id, limit: 500)
      results["results"].each do |result, index|
        nice_date = DateTime.parse(result["start_time"]).strftime("%b %d")
        events[result["name"] + " - " + nice_date] = result["id"]
      end
      $cache[:events][id] = {'items' => events, 'timestamp' => Time.now}
      return events
    end 
  end

  def self.rsvp_person_to_event(person_id, event_id)
    client = NationBuilder::Client.new(NATION, API_KEY)
    rsvp = {
      "person_id" => person_id,
      "guests_count" => 0,
      "volunteer" => false,
      "private" => true,
      "canceled" => false,
      "attended" => false
    }
    client.call(:events, :rsvp_create, site_slug: SITE_SLUG, id: event_id, rsvp: rsvp)
  end

end

class EventShimServlet < WEBrick::HTTPServlet::AbstractServlet
  def do_GET (request, response)
    case request.path
      when "/calendars"
        calendars = NationBuilderInterface::get_calendars
        response.body = calendars.to_json
      when "/events"
        if request.query["calendar_id"]
          events = NationBuilderInterface::get_events_by_calendar(request.query["calendar_id"]) 
          response.body = events.to_json
        else
          events = NationBuilderInterface::get_all_events
          response.body = events.to_json
        end
      when "/rsvp"
        if request.query["person_id"] && request.query["event_id"]
          NationBuilderInterface::rsvp_person_to_event(request.query["person_id"], request.query["event_id"])
          response.body = "OK"
        end
    end
    response.status = 200
    response.header["Access-Control-Allow-Origin"] = "*"
  end
end

cert_name = [
  %w[CN localhost],
]


server = WEBrick::HTTPServer.new(
  :Port => 8555,
  :SSLEnable => true,
  :SSLCertName => cert_name
  )

server.mount "/", EventShimServlet

trap("INT") {
  server.shutdown
}

server.start
